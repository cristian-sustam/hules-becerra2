<?php

namespace App;

use App\CarruselHasProductos;
use App\Product;

use Illuminate\Database\Eloquent\Model;

class Carrusel extends Model
{
    public function productos()
    {
    	$ids_pro = CarruselHasProductos::where('carrusel_id',$this->id)->pluck('product_id');
    	$this->productos = Product::whereIn('id',$ids_pro)->get();
    }
}
