<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image1'=>'image|mimes:jpeg,bmp,png,jpg,gif',
            'image2'=>'image|mimes:jpeg,bmp,png,jpg,gif',
            'image3'=>'image|mimes:jpeg,bmp,png,jpg,gif',
            'image4'=>'image|mimes:jpeg,bmp,png,jpg,gif'
        ];
    }
}
