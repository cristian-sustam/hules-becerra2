<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplicationsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id.value'=>'required|numeric',
            'product_id'=>'required|numeric',
            'model_id.value'=>'required|numeric',
            'motor'=>'required|min:1',
            'year_min'=>'required|min:4'
        ];
    }
}
