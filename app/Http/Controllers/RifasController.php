<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rifa;

class RifasController extends Controller
{
    public function index() //Este es para el admin
    {
      $rifas = Rifa::all();
      return response()->json($rifas);
    }

    public function show($id)
    {
      $rifa = Rifa::find($id);
      return response()->json($rifa);
    }
    public function store(Request $request)
    {
    	$rifa = Rifa::where('codigo',$request->codigo)->first();
    	if($rifa)
    		return response()->json(['code' => -1]);

      	$rifa = new Rifa();
      	$rifa->codigo = $request->codigo;
      	$rifa->nombre = $request->nombre;
      	$rifa->correo = $request->correo;
      	$rifa->telefono = $request->telefono;
      	$rifa->save();
      	return response()->json(['code' => 1, 'rifa' => $rifa]);
    }
    public function update(Request $request, $id)
    {
		$rifa = Rifa::where('codigo',$request->codigo)->first();
		if($rifa)
			if($rifa->id != $id)
				return -1;

      	$rifa = Rifa::find($id);
 		$rifa->codigo = $request->codigo;
      	$rifa->nombre = $request->nombre;
      	$rifa->correo = $request->correo;
      	$rifa->telefono = $request->telefono; 		
      	$rifa->save();
      	return 1;
    }
    public function destroy($id)
     {
       if($this->_deleteRifa($id)){
           return response()->json(['msg'=>'Rifa con ID '.$id.' eliminada.']);
       }
       else{
           return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
       }
     }

     public function destroyMultiple(Request $request)
     {
         foreach ($request->ids as $key => $value) {
             $status=$this->_deleteRifa($value);
             if(!$status)
                 break;
         }

         if ($status) {
             return response()->json(['msg'=>'Rifas eliminadas.']);
         }
         else{
             return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
         }
     }

     private function _deleteRifa($rifa_id)
     {
       $rifa = Rifa::find($rifa_id);

       if ($rifa->delete()) {
           return true;
       }
       else{
           return false;
       }
     }
}
