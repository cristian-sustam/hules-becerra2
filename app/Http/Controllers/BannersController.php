<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use App\Http\Requests\BannersFormRequest;
use Images;

class BannersController extends Controller
{
    public function index()
    {
      $banners=Banner::all();
      $data=[];
      foreach ($banners as $key => $value) {
          $value->image=Images::getImg($value->image_id);
          array_push($data, $value);
      }
      return response()->json($data);
    }
    public function indexHome()
    {
      $banners=Banner::all();
      $data=[];
      foreach ($banners as $key => $value){
          $value->imageUrl=Images::getUrl($value->image_id);
          array_push($data, $value);
      }
      return response()->json($data);
    }
    public function show($id)
    {
      $banner=Banner::find($id);
      $banner->imageUrl=Images::getUrl($banner->image_id);
      return response()->json($banner);
    }
    public function store(BannersFormRequest $request)
    {
      $banner = new Banner();
      $banner->name = $request->name;
      if($request->image){
          $image_id=Images::save($request->image);
          $banner->image_id=$image_id;
      }
      $banner->save();
      $banner->img;
      return response()->json($banner);
    }
    public function update(BannersFormRequest $request, $id)
    {
      $banner=Banner::find($id);
      $banner->name=$request->name;

      if(isset($request->image)){
          if ($banner->image_id!=1) {
              //Borramos la imagen anterior
              Images::delete($banner->image_id);
          }
          //Subimos la nueva imagen
          $image_id=Images::save($request->image);
          $banner->image_id=$image_id;
      }

      $banner->save();
      $banner->img;
      return response()->json($banner->id);
    }
    public function destroy($id)
     {
       if($this->_deleteBanner($id))
           return response()->json(['msg'=>'Banner con ID '.$id.' eliminado.']);
       else
           return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     public function destroyMultiple(Request $request)
     {
         foreach ($request->ids as $key => $value)
         {
             $status=$this->_deleteBanner($value);
             if(!$status)
                 break;
         }
         if ($status) 
             return response()->json(['msg'=>'Banners eliminados.']);
         else
             return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     private function _deleteBanner($banner_id)
     {
       $banner=Banner::find($banner_id);
       if($banner->image_id != 1)
           Images::delete($banner->image_id);
       if ($banner->delete())
           return true;
       else
           return false;
     }
}
