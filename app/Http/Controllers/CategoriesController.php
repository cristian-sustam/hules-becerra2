<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

use Images;
class CategoriesController extends Controller
{
  public function categorias(){
    $categorias = Category::all();
    foreach ($categorias as $key => $value)
      $value->imagen = Images::getImg($value->image_id);
    return response()->json($categorias);
  }
  public function mostrarCategoria($id){
    $categoria = Category::find($id);
    $categoria->imageUrl = Images::getUrl($categoria->image_id);
    return response()->json($categoria);
  }
  public function actualizarCategoria(Request $request,$id){
    $categoria = Category::find($id);

    if($request->image)
    {
      //Subimos la nueva imagen
      $image_id = Images::save($request->image);

      if ($categoria->image_id != 5) //Borramos la imagen anterior
          Images::delete($categoria->image_id);

      $categoria->image_id = $image_id;
    }
    $categoria->save();
    return response()->json($categoria);
  }



    public function index()
    {
      $categories=Category::all();
      return response()->json($categories);
    }
    public function show($id)
    {
      $category=Category::find($id);
      return response()->json($category);
    }
    public function store(Request $request)
    {
      $name = Category::where('name',$request->name)->first();
      if($name)
      	return response()->json(['msg'=>'Esa castegoria ya existe']);
      $category = new Category();
      $category->name = $request->name;
      $category->save();
      return response()->json($category);
    }
    public function update(Request $request, $id)
    {
      $name = Category::where('name',$request->name)->first();
      if($name)
      	if($name->id!=$id)
      		return response()->json(['msg'=>'Esa castegoria ya existe']);
      $category=Category::find($id);
      $category->name=$request->name;
      $category->save();
      return response()->json($category->id);
    }
    public function destroy($id)
     {
       if($this->_deleteCategory($id))
           return response()->json(['msg'=>'Categoria con ID '.$id.' eliminada.']);
       else
           return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     public function destroyMultiple(Request $request)
     {
         foreach ($request->ids as $key => $value)
         {
             $status=$this->_deleteCategory($value);
             if(!$status)
                 break;
         }
         if ($status) 
             return response()->json(['msg'=>'Categorias eliminadas.']);
         else
             return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     private function _deleteCategory($category_id)
     {
       $category=Category::find($category_id);
       if ($category->delete()) 
           return true;
       else
           return false;
     }
}
