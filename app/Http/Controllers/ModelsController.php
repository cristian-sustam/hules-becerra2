<?php

namespace App\Http\Controllers;

use App\Models;
use App\Brand;
use Illuminate\Http\Request;

class ModelsController extends Controller
{
    public function index()
    {
      $models=Models::all();
      foreach ($models as $key => $value)
        $value->brand;
      return response()->json($models);
    }
    public function show($id)
    {
      $model=Models::find($id);
      $model->brand;
      return response()->json($model);
    }
    public function showBrand($id)
    {
      $models=Models::where('brand_id',$id)->orderBy('name','ASC')->get();
      return response()->json($models);
    }
    public function store(Request $request)
    {
      $name = Models::where('name',$request->name)->first();
      if($name)
        if($name->brand_id == $request->brand_id['value'])
      	  return response()->json(['msg'=>'El nombre del modelo ya existe para esa marca']);

      $model = new Models();
      $model->name = $request->name;
      $model->brand_id = $request->brand_id['value'];
      $model->save();
      return response()->json($model);
    }
    public function update(Request $request, $id)
    {
      $name = Models::where('name',$request->name)->first();
      if($name)
      	if($name->id!=$id && $name->brand_id == $request->brand_id['value'])
      		return response()->json(['msg'=>'El nombre del modelo ya existe para esa marca']);

      $model=Models::find($id);
      $model->name=$request->name;
      $model->brand_id = $request->brand_id['value'];
      $model->save();
      return response()->json($model->id);
    }
    public function destroy($id)
     {
       if($this->_deleteModel($id))
           return response()->json(['msg'=>'Modelo con ID '.$id.' eliminado.']);
       else
           return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     public function destroyMultiple(Request $request)
     {
         foreach ($request->ids as $key => $value)
         {
             $status=$this->_deleteModel($value);
             if(!$status)
                 break;
         }

         if ($status) 
             return response()->json(['msg'=>'Modelos eliminados.']);
         else
             return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     private function _deleteModel($model_id)
     {
       $model=Models::find($model_id);
       if ($model->delete())
           return true;
       else
           return false;
     }
}
