<?php

namespace App\Http\Controllers;

use App\Product;
use App\Models;
use App\Category;
use App\Application;
use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Requests\ProductsFormRequest;
use Images;
use Mail;
//Para saber si las imagenes existen o no.
use Illuminate\Support\Facades\Storage;
class ProductsFrontController extends Controller
{
    public function automotiveIndex($perPage)
    {
        $automotriz = Category::select('image_id')->where('name','Automotriz')->first();
        $automotriz->imageUrl = Images::getUrl($automotriz->image_id);

    	$products = Product::where('category_id',(Category::where('name','automotriz')->first())->id)->orderBy('position','asc')->paginate($perPage);

    	foreach ($products as $key => $value)
        {
    		$value->subcategory;
            if($value->image1 != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($value->image1_id)->path))
                    $value->image1=Images::getUrl($value->image1_id);
                else
                    $value->image1=Images::getUrl(5);
            }
            else
                $value->image1=Images::getUrl(5);

    	}
        $data = ['imageUrl' => $automotriz->imageUrl, 'products' => $products];
    	return response()->json($data);
    }
    public function empaqueIndex($perPage)
    {
        $empaque = Category::select('image_id')->where('name','Empaque')->first();
        $empaque->imageUrl = Images::getUrl($empaque->image_id);

        $products = Product::where('category_id',(Category::where('name','empaque')->first())->id)->orderBy('position','asc')->paginate($perPage);
        
        foreach ($products as $key => $value)
        {
            $value->subcategory;

            if($value->image1 != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($value->image1_id)->path))
                    $value->image1=Images::getUrl($value->image1_id);
                else
                    $value->image1=Images::getUrl(5);
            }
            else
                $value->image1=Images::getUrl(5);
        }
        $data = ['imageUrl' => $empaque->imageUrl, 'products' => $products];
        return response()->json($data);
    }
    public function industrialIndex($perPage)
    {
        $industrial = Category::select('image_id')->where('name','Industrial')->first();
        $industrial->imageUrl = Images::getUrl($industrial->image_id);

        $products = Product::where('category_id',(Category::where('name','industrial')->first())->id)->orderBy('position','asc')->paginate($perPage);

        foreach ($products as $key => $value)
        {
            $value->subcategory;

            if($value->image1 != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($value->image1_id)->path))
                    $value->image1=Images::getUrl($value->image1_id);
                else
                    $value->image1=Images::getUrl(5);
            }
            else
                $value->image1=Images::getUrl(5);

        }
        $data = ['imageUrl' => $industrial->imageUrl, 'products' => $products];
        return response()->json($data);
    }
    public function heavyIndex($perPage)
    {
        $pesado = Category::select('image_id')->where('name','Equipo pesado')->first();
        $pesado->imageUrl = Images::getUrl($pesado->image_id);

        $products = Product::where('category_id',(Category::where('name','equipo pesado')->first())->id)->orderBy('position','asc')->paginate($perPage);

        foreach ($products as $key => $value)
        {
            $value->subcategory;

            if($value->image1 != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($value->image1_id)->path))
                    $value->image1=Images::getUrl($value->image1_id);
                else
                    $value->image1=Images::getUrl(5);
            }
            else
                $value->image1=Images::getUrl(5);
        }

        $data = ['imageUrl' => $pesado->imageUrl, 'products' => $products];
        return response()->json($data);
    }
    public function grapaIndex($perPage)
    {
        $grapa = Category::select('image_id')->where('name','Grapa automotriz')->first();
        $grapa->imageUrl = Images::getUrl($grapa->image_id);

        $products = Product::where('category_id',(Category::where('name','grapa automotriz')->first())->id)->orderBy('position','asc')->paginate($perPage);
    
        foreach ($products as $key => $value)
        {
            $value->subcategory;

            if($value->image1 != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($value->image1_id)->path))
                    $value->image1=Images::getUrl($value->image1_id);
                else
                    $value->image1=Images::getUrl(5);
            }
            else
                $value->image1=Images::getUrl(5);
        }
        $data = ['imageUrl' => $grapa->imageUrl, 'products' => $products];
        return response()->json($data);
    }
    public function universalIndex($perPage)
    {
        $uni = Category::select('image_id')->where('name','Universal')->first();
        $uni->imageUrl = Images::getUrl($uni->image_id);

        $products = Product::where('category_id',(Category::where('name','universal')->first())->id)->orderBy('position','asc')->paginate($perPage);

        foreach ($products as $key => $value)
        {
            $value->subcategory;

            if($value->image1 != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($value->image1_id)->path))
                    $value->image1=Images::getUrl($value->image1_id);
                else
                    $value->image1=Images::getUrl(5);
            }
            else
                $value->image1=Images::getUrl(5);
        }
        $data = ['imageUrl' => $uni->imageUrl, 'products' => $products];
        return response()->json($data);
    }
   
    public function otrosIndex($perPage)
    {
        $otros = Category::select('image_id')->where('name','otros')->first();
        $otros->imageUrl = Images::getUrl($otros->image_id);

        $products = Product::where('category_id',(Category::where('name','otros')->first())->id)->orderBy('position','asc')->paginate($perPage);
        
        foreach ($products as $key => $value)
        {
            $value->subcategory;

            if($value->image1 != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($value->image1_id)->path))
                    $value->image1=Images::getUrl($value->image1_id);
                else
                    $value->image1=Images::getUrl(5);
            }
            else
                $value->image1=Images::getUrl(5);
        }
        $data = ['imageUrl' => $otros->imageUrl, 'products' => $products];
        return response()->json($data);
    }
    public function add($id)
    {
        $data = Product::find($id);
        if($data)
        {
            if($data->image1_id != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($data->image1_id)->path))
                    $data->image=Images::getUrl($data->image1_id);
                else
                    $data->image=Images::getUrl(5);
            }
            else
                $data->image1=Images::getUrl(5);
            $data->subcategory;
        }
        return response()->json($data);

    }
    public function show($id)
    {
    	$product = Product::find($id);
    	if($product)
    	{
            if($product->image1 != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($product->image1_id)->path))
                    $product->image1=Images::getUrl($product->image1_id);
                else
                    $product->image1=Images::getUrl(5);
            }
            else
                $product->image1=Images::getUrl(5);

            if($product->image2 != 5)                
                if(Storage::disk('public')->exists(Images::get($product->image2_id)->path))
                    $product->image2=Images::getUrl($product->image2_id);
                else
                    $product->image2_id = 5;

            if($product->image3 != 5)                
                if(Storage::disk('public')->exists(Images::get($product->image3_id)->path))
                    $product->image3=Images::getUrl($product->image3_id);
                else
                    $product->image3_id = 5;

            if($product->image4 != 5)                
                if(Storage::disk('public')->exists(Images::get($product->image4_id)->path))
                    $product->image4=Images::getUrl($product->image4_id);
                else
                    $product->image4_id = 5;

            //Algunas de las imagenes contienen en espacios (esto es una solucion temporal)
                //Delimitador \ + espacio en blanco
            $product->image1 = str_replace(' ', '\ ',$product->image1);
            $product->image2 = str_replace(' ', '\ ',$product->image2);
            $product->image3 = str_replace(' ', '\ ',$product->image3);
            $product->image4 = str_replace(' ', '\ ',$product->image4);

            $product->apps;
            foreach ($product->apps as $key => $value)
            {
                $value->brand;
                $value->model;
            }
            if($product->equivalence1 || $product->equivalence2 ||$product->equivalence3)
                $product->equivalence = true;
            else
                $product->equivalence = false;
    		$product->category;
    		$product->subcategory;		
    	}
    	return response()->json($product);
    }

    public function advanceFilter(Request $request, $perPage)
    {   
        $productos = Product::select('id','image1_id','name','sku','subcategory_id','position');

        if($request->tipoBusqueda == 'automotriz') //Verificamos si la busqueda fue por 'Automotriz' o si fue 'OTROS'
        {
            $apps = Application::select('product_id'); //Iniciamos la consulta con el parametro que necesitamos para ir concatenando los where.

            if($request->year != null)
                $apps = $apps->where('year_min','<=',$request->year)
                                    ->where(function($query) use ($request){
                                        $query->where('year_max','>=', $request->year)
                                              ->orWhere('year_max',null);
                                    });


            if($request->motor != null)
            {
                $a = Application::select('motor')->where('id',$request->motor)->first();
                $apps = $apps->where('motor','LIKE',"%".$a->motor."%");
            }
            

            if($request->model != null)
                $apps = $apps->where('model_id',$request->model);
            else if($request->brand != null)
                $apps = $apps->where('brand_id',$request->brand);

            if($request->keywords != '')
            {
                $models = Models::where('name','LIKE',"%".$request->keywords."%")->pluck('id');
                $apps = $apps->whereIn('model_id',$models);
            }
                
            $apps = $apps->distinct('product_id')->pluck('product_id');

            $productos = $productos->whereIn('id',$apps);

            if($request->subcategory != null)
                $productos = $productos->where('subcategory_id',$request->subcategory);
            
            if($request->sku != '')
                $productos = $productos->where('sku','LIKE',"%".$request->sku."%");

            if($request->keywords != '')
                $productos = $productos->where('sku','LIKE','%'.$request->keywords.'%')->orWhere('name','LIKE','%'.$request->keywords.'%');
        }
        else
        {
            if($request->subcategoryOtros != null)
                $productos = $productos->where('subcategory_id',$request->subcategoryOtros);

            if($request->sku != '')
                $productos = $productos->where('sku','LIKE',"%".$request->sku."%");

            if($request->keywords != '')
                $productos = $productos->where('sku','LIKE','%'.$request->keywords.'%')->orWhere('name','LIKE','%'.$request->keywords.'%');
        }

        $productos = $productos->orderBy('position','ASC')->paginate($perPage);

        foreach ($productos as $key => $value)
        {
            $value->subcategory;

            if($value->image1_id != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($value->image1_id)->path))
                    $value->image1=Images::getUrl($value->image1_id);
                else
                    $value->image1=Images::getUrl(5);
            }
            else
                $value->image1=Images::getUrl(5);
        }
        return response()->json($productos);
    }
    public function simpleFilter(Request $request, $perPage)
    {
        $products = Product::select('products.*','sub_categories.name as subcategory')->
                             join('sub_categories','products.subcategory_id','=','sub_categories.id')->
                             join('categories','products.category_id','=','categories.id')->
                             where('products.sku','LIKE','%'.$request->keywords.'%')->orWhere('products.name','LIKE','%'.$request->keywords.'%')->orWhere('categories.name','LIKE','%'.$request->keywords.'%')->orWhere('sub_categories.name','LIKE','%'.$request->keywords.'%')->orderBy('position','asc')->paginate($perPage);

        foreach ($products as $key => $value)
        {
            $value->subcategory;

            if($value->image1 != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($value->image1_id)->path))
                    $value->image1=Images::getUrl($value->image1_id);
                else
                    $value->image1=Images::getUrl(5);
            }
            else
                $value->image1=Images::getUrl(5);
        }

        return response()->json($products);
    }
    public function promos()
    {
        $products = Product::orderBy('created_at','desc')->take(10)->get(); // where promos en el futuro
        $data=[];

        /*if(Storage::disk('public')->exists('photos/default.png'))
            echo "El fichero existe";
        else
            echo "El fichero no existe";*/
        foreach ($products as $key => $value) {
            $value->subcategory;

            if($value->image1 != 5)
            {                
                if(Storage::disk('public')->exists(Images::get($value->image1_id)->path))
                    $value->image1=Images::getUrl($value->image1_id);
                else
                    $value->image1=Images::getUrl(5);
            }
            else
                $value->image1=Images::getUrl(5);

            array_push($data, $value);
        }
        return response()->json($data);
    }
}
