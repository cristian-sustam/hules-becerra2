<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;
use App\Http\Requests\BrandsFormRequest;
use Images;

class BrandsController extends Controller
{
    public function index() //Este es para el admin
    {
      $brands=Brand::all();
      $data=[];
      foreach ($brands as $key => $value) {
          $value->image=Images::getImg($value->image_id);
          array_push($data, $value);
      }
      return response()->json($data);
    }
    public function indexAlfabeticamente() //Este es para el header
    {
      $brands=Brand::orderBy('name','ASC')->get();
      $data=[];
      foreach ($brands as $key => $value) {
          $value->image=Images::getImg($value->image_id);
          array_push($data, $value);
      }
      return response()->json($data);
    }
    public function show($id)
    {
      $brand=Brand::find($id);
      $brand->imageUrl=Images::getUrl($brand->image_id);
      return response()->json($brand);
    }
    public function store(BrandsFormRequest $request)
    {
      $brand = new Brand();
      $brand->name = $request->name;
      if($request->image){
          $image_id=Images::save($request->image);
          $brand->image_id=$image_id;
      }
      $brand->save();
      $brand->img;
      return response()->json($brand);
    }
    public function update(BrandsFormRequest $request, $id)
    {
      $brand=Brand::find($id);
      $brand->name=$request->name;

      if(isset($request->image)){
          if ($brand->image_id!=1) {
              //Borramos la imagen anterior
              Images::delete($brand->image_id);
          }
          //Subimos la nueva imagen
          $image_id=Images::save($request->image);
          $brand->image_id=$image_id;
      }

      $brand->save();
      $brand->img;
      return response()->json($brand->id);
    }
    public function destroy($id)
     {
       if($this->_deleteBrand($id)){
           return response()->json(['msg'=>'Marca con ID '.$id.' eliminado.']);
       }
       else{
           return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
       }
     }

     public function destroyMultiple(Request $request)
     {
         foreach ($request->ids as $key => $value) {
             $status=$this->_deleteBrand($value);
             if(!$status)
                 break;
         }

         if ($status) {
             return response()->json(['msg'=>'Marcas eliminadas.']);
         }
         else{
             return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
         }
     }

     private function _deleteBrand($brand_id)
     {
       $brand=Brand::find($brand_id);
       if($brand->image_id != 1){
           Images::delete($brand->image_id);
       }
       if ($brand->delete()) {
           return true;
       }
       else{
           return false;
       }
     }
}
