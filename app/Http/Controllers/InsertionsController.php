<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\InsertionsFormRequest;
use Illuminate\Support\Facades\Storage;
use App\Product;
use App\Application;
use App\Models;
use App\Category;
use App\SubCategory;
use App\Brand;
use App\Image;

class InsertionsController extends Controller
{
    public function insert(InsertionsFormRequest $request)
    {
    	$file = $request->file('file');
    	$registros = array();
    	$data=[]; //Arrglo para acumular los estados de cada registro, osea si se inserto o tuvo problema)s

		if (($fichero = fopen($file, "r")) !== FALSE) 
		{
			//Lee los nombres de los campos
			$nombres_campos = fgetcsv($fichero, 0, ",", "\"", "\"");

			//Quita los espacios en blanco.
		    if(!empty($nombres_campos))
		    	foreach ($nombres_campos as $key => &$nombre) 
		    		$nombre = strtolower(trim($nombre));		    	

		    $num_campos = count($nombres_campos);//numero de los campos;

		    // Lee los registros
		    while (($datos = fgetcsv($fichero, 0, ",", "\"", "\"")) !== FALSE) {
		    	
		        // Crea un array asociativo con los nombres y valores de los campos
		        for ($icampo = 0; $icampo < $num_campos; $icampo++) {
		            
		            $registro[$nombres_campos[$icampo]] = (isset($datos[$icampo])) ? utf8_encode(trim($datos[$icampo])) : '';
		        }
		        // Añade el registro leido al array de registros
		        $registros[] = $registro;
		    }
		    fclose($fichero);

		    $cont_exitos = 0;
		    $cont_fracasos = 0;
		    $cont_repetidos = 0;
		    $campos_completos = true;
		    if(!array_key_exists('sku',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Sku"'];
		    }
		    if(!array_key_exists('nombre',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Nombre"'];
		    }
		    /*if(!array_key_exists('imagen 1',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Imagen 1"'];
		    }
		    if(!array_key_exists('imagen 2',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Imagen 2"'];
		    }
		    if(!array_key_exists('imagen 3',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Imagen 3"'];
		    }
		    if(!array_key_exists('imagen 4',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Imagen 4"'];
		    }*/
		    if(!array_key_exists('marca',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Marca"'];
		    }
		    if(!array_key_exists('modelo',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Modelo"'];
		    }
		    if(!array_key_exists('motor',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Motor"'];
		    }
		    if(!array_key_exists('categoria',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Categoria"'];
		    }
		    if(!array_key_exists('subcategoria',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Subcategoria"'];
		    }
		    if(!array_key_exists('anio inicio',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Anio inicio"'];
		    }
		    if(!array_key_exists('anio fin',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Anio fin"'];
		    }
		    
		    if(!array_key_exists('especificaciones',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Especificaciones"'];
		    }
		    if(!array_key_exists('existencia',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Existencia"'];
		    }
		    if(!array_key_exists('equivalencia 1',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Equivalencia 1"'];
		    }
		    if(!array_key_exists('equivalencia 2',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Equivalencia 2"'];
		    }
		    if(!array_key_exists('equivalencia 3',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Equivalencia 3"'];
		    }
		    if($campos_completos)
		    {
		    	foreach ($registros as $key => $registro)
			    {
			    	if($registro['sku'] != '' &&  $registro['marca'] !='' && $registro['modelo']!='' && $registro['motor']!='' && $registro['categoria']!= '' && $registro['subcategoria']!= '' && $registro['anio inicio']!= '')
			    	{     
		    	    	$categoria = Category::where('name',$registro['categoria'])->first();
				    	if($categoria)
				    	{
				    		$product = Product::where('sku',$registro['sku'])->first();
				    		if(!$product)
				    		{
				    			$product = new Product();
				    			$product->sku = $registro['sku'];
				    			$product->name = $registro['nombre'];
				    			$product->category_id = $categoria->id;
				    			$subcategoria = SubCategory::where('category_id',$categoria->id)->where('name',$registro['subcategoria'])->first();
				    			if(!$subcategoria)
				    			{
				    				$subcategoria = new SubCategory();
				    				$subcategoria->name = $registro['subcategoria'];
				    				$subcategoria->category_id = $categoria->id;
				    				$subcategoria->save();
				    			}
				    			$product->subcategory_id = $subcategoria->id;
				    			if(is_numeric($registro['existencia']))
				    				$product->stock = $registro['existencia'];

				    			$product->equivalence1 = $registro['equivalencia 1'];
				    			$product->equivalence2 = $registro['equivalencia 2'];
				    			$product->equivalence3 = $registro['equivalencia 3'];

				    			$image = new Image();
                                $image->path = "photos/". $product->sku ."a.jpg";
                                $image->disk = "public";
                                $image->key = "".$product->sku."a";
                                $image->save();
                                $product->image1_id = $image->id;

                                $image2 = new Image();
                                $image2->path = "photos/". $product->sku ."b.jpg";
                                $image2->disk = "public";
                                $image2->key = "".$product->sku."b";
                                $image2->save();
                                $product->image2_id = $image2->id;

                                $image3 = new Image();
                                $image3->path = "photos/". $product->sku ."c.jpg";
                                $image3->disk = "public";
                                $image3->key = "".$product->sku."c";
                                $image3->save();
                                $product->image3_id = $image3->id;

                                $image4 = new Image();
                                $image4->path = "photos/". $product->sku ."d.jpg";
                                $image4->disk = "public";
                                $image4->key = "".$product->sku."d";
                                $image4->save();
                                $product->image4_id = $image4->id;
					    		$product->specifications = $registro['especificaciones'];
					    		
				    			if($product->save())
				    				$data[] = ['msg'=>'FILA '.($key+1).' Producto ('.$registro['sku'].') Agregado correctamente'];

				    		}
				    		$app = new Application();
				    		$app->product_id = $product->id;
					    	$marca = Brand::where('name',$registro['marca'])->first();
					    	if(!$marca)
					    	{
					    		$marca = new Brand();
					    		$marca->name = $registro['marca'];
					    		$marca->save();
					    	}
					    	$app->brand_id = $marca->id;
					    	$modelo = Models::where('name',$registro['modelo'])->where('brand_id',$marca->id)->first();
					    	if(!$modelo)
					    	{
					    		$modelo = new Models();
					    		$modelo->name = $registro['modelo'];
					    		$modelo->brand_id = $marca->id;
					    		$modelo->save();
					    	}
					    	$app->model_id = $modelo->id;

					    	if(is_numeric($registro['anio inicio']))
					    		$app->year_min = $registro['anio inicio'];
					    	if(is_numeric($registro['anio fin']))
					    		$app->year_max = $registro['anio fin'];

					    	$app->motor = $registro['motor'];
					    	if($app->save())
					    	{
					    		$data[] = ['msg'=>'FILA '.($key+1).' Aplicación para el producto ('.$registro['sku'].') agregada correctamente'];
					    		$cont_exitos++;
					    	}
				    	}
				    	else
				    	{
				    		$data[] = ['msg'=>'FILA '.($key+1).' No existe esa categoria'];
				    		$cont_fracasos++;
				    	}
				    }
				    else
				    {
				    	$data[] = ['msg'=>'FILA '.($key+1).' Tiene campos incompletos'];
				    	$cont_fracasos++;
				    }
			    }
		    }
		}
		else
			$data[] = ['msg' =>'No se pudo abirir el archivo'];

    	return response()->json($data);
    }

    public function insertOtros(InsertionsFormRequest $request)
    {
    	$file = $request->file('file');
    	$registros = array();
    	$data=[]; //Arrglo para acumular los estados de cada registro, osea si se inserto o tuvo problema)s

		if (($fichero = fopen($file, "r")) !== FALSE) 
		{
			//Lee los nombres de los campos
			$nombres_campos = fgetcsv($fichero, 0, ",", "\"", "\"");

			//Quita los espacios en blanco.
		    if(!empty($nombres_campos))
		    	foreach ($nombres_campos as $key => &$nombre) 
		    		$nombre = strtolower(trim($nombre));		    	

		    $num_campos = count($nombres_campos);//numero de los campos;

		    // Lee los registros
		    while (($datos = fgetcsv($fichero, 0, ",", "\"", "\"")) !== FALSE) {
		    	
		        // Crea un array asociativo con los nombres y valores de los campos
		        for ($icampo = 0; $icampo < $num_campos; $icampo++) {
		            
		            $registro[$nombres_campos[$icampo]] = (isset($datos[$icampo])) ? utf8_encode(trim($datos[$icampo])) : '';
		        }
		        // Añade el registro leido al array de registros
		        $registros[] = $registro;
		    }
		    fclose($fichero);

		    $cont_exitos = 0;
		    $cont_fracasos = 0;
		    $cont_repetidos = 0;
		    $campos_completos = true;
		    if(!array_key_exists('sku',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Sku"'];
		    }
		    if(!array_key_exists('nombre',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Nombre"'];
		    }
		    if(!array_key_exists('categoria',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Categoria"'];
		    }
		    if(!array_key_exists('subcategoria',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Subcategoria"'];
		    }	    
		    if(!array_key_exists('especificaciones',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Especificaciones"'];
		    }
		    if(!array_key_exists('existencia',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Existencia"'];
		    }
		    if(!array_key_exists('equivalencia 1',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Equivalencia 1"'];
		    }
		    if(!array_key_exists('equivalencia 2',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Equivalencia 2"'];
		    }
		    if(!array_key_exists('equivalencia 3',$registro))
		    {
		    	$campos_completos = false;
		    	$data[] = ['msg'=>'No se encontró la columna "Equivalencia 3"'];
		    }
		    if($campos_completos)
		    {
		    	foreach ($registros as $key => $registro)
			    {
			    	if($registro['sku'] != '' &&  $registro['categoria']!= '' && $registro['subcategoria']!= '')
			    	{     
		    	    	$categoria = Category::where('name',$registro['categoria'])->where('name','!=','Automotriz')->first();
				    	if($categoria)
				    	{
				    		$product = Product::where('sku',$registro['sku'])->first();
				    		if(!$product)
				    		{
				    			$product = new Product();
				    			$product->sku = $registro['sku'];
				    			$product->name = $registro['nombre'];
				    			$product->category_id = $categoria->id;
				    			$subcategoria = SubCategory::where('category_id',$categoria->id)->where('name',$registro['subcategoria'])->first();
				    			if(!$subcategoria)
				    			{
				    				$subcategoria = new SubCategory();
				    				$subcategoria->name = $registro['subcategoria'];
				    				$subcategoria->category_id = $categoria->id;
				    				$subcategoria->save();
				    			}
				    			$product->subcategory_id = $subcategoria->id;
				    			if(is_numeric($registro['existencia']))
				    				$product->stock = $registro['existencia'];

				    			$product->equivalence1 = $registro['equivalencia 1'];
				    			$product->equivalence2 = $registro['equivalencia 2'];
				    			$product->equivalence3 = $registro['equivalencia 3'];

				    			$image = new Image();
                                $image->path = "photos/". $product->sku ."a.jpg";
                                $image->disk = "public";
                                $image->key = "".$product->sku."a";
                                $image->save();
                                $product->image1_id = $image->id;

                                $image2 = new Image();
                                $image2->path = "photos/". $product->sku ."b.jpg";
                                $image2->disk = "public";
                                $image2->key = "".$product->sku."b";
                                $image2->save();
                                $product->image2_id = $image2->id;

                                $image3 = new Image();
                                $image3->path = "photos/". $product->sku ."c.jpg";
                                $image3->disk = "public";
                                $image3->key = "".$product->sku."c";
                                $image3->save();
                                $product->image3_id = $image3->id;

                                $image4 = new Image();
                                $image4->path = "photos/". $product->sku ."d.jpg";
                                $image4->disk = "public";
                                $image4->key = "".$product->sku."d";
                                $image4->save();
                                $product->image4_id = $image4->id;
					    		$product->specifications = $registro['especificaciones'];
					    		
				    			if($product->save())
				    				$data[] = ['msg'=>'FILA '.($key+1).' Producto ('.$registro['sku'].') Agregado correctamente'];

				    		}
				    		else
				    		{
				    			$data[] = ['msg'=>'FILA '.($key+1).' El producto con el sku: '.$registro['sku'].' ya existe'];
				    			$cont_fracasos++;
				    		}
				    	}
				    	else
				    	{
				    		$data[] = ['msg'=>'FILA '.($key+1).' No existe esa categoria'];
				    		$cont_fracasos++;
				    	}
				    }
				    else
				    {
				    	$data[] = ['msg'=>'FILA '.($key+1).' Tiene campos incompletos'];
				    	$cont_fracasos++;
				    }
			    }
		    }
		}
		else
			$data[] = ['msg' =>'No se pudo abirir el archivo'];

    	return response()->json($data);
    }
}

/*	$name = time().$file->getClientOriginalName();
    $file->move(public_path(),$name);
*/