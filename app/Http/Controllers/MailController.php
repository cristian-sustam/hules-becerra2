<?php

namespace App\Http\Controllers;

use Mail;
use App\Boletin;
use App\Rifa;
use App\Product;
use App\Category;
use App\SubCategory;
use Images;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail as Correo;
use App\Mail\Cotizacion;
use App\Mail\Guarantee;
use App\Mail\Contac;
use App\Mail\NuevaRifa;
use App\Mail\Boletin as Boletines;
use App\Mail\NuevoBoletin;

class MailController extends Controller
{
    public function contact(Request $request)
    {
    	$data = ['name' => $request->name,
                 'email' => $request->email,                    
                 'text'=> $request->text,
                 'subject'=>$request->subject];

    	/*Mail::send('emails.message', $data ,function($message) use ($subject)
        {
            $message->from('sandbox@internetizante.com', 'Hules Becerra');
            $message->to('cristian@sustam.com')->subject($subject);
        });*/
        Correo::to(env("CONTACTO_EMAIL"))->send(new Contac($data));
        return response()->json(['msg'=>'Mensaje enviado']);
    }
    public function warranty(Request $request)
    {
        $data = ['fecha'=>$request->fecha,
                 'numero'=>$request->numero,
                 'codigo'=>$request->codigo,
                 'problema'=>$request->problema,
                 'nombre'=>$request->nombre,
                 'comentario' => $request->comentario];

        /*Mail::send('emails.warranty',$data, function($message){

            $message->from('sandbox@internetizante.com','Hules Becerra');
            $message->to('cristian@sustam.com')->subject('Garantia');

        });*/
        Correo::to(env("CONTACTO_EMAIL"))->send(new Guarantee($data));
        return response()->json(['msg'=>'Mensaje enviado']);
    }
    public function cotizacion(Request $request)
    {
        $data = ['nombre'=>$request->customer['name'],
                 'correo'=>$request->customer['email'],
                 'telefono'=>$request->customer['telphone'],
                 'productos'=>$request->productos];

        /*Mail::send('emails.cotizacion',$data, function($message){

            $message->from('sandbox@internetizante.com','Hules Becerra');
            $message->to('cristian@sustam.com')->subject('Cotización');

        });*/
        Correo::to(env("COTIZACIONES_EMAIL"))->send(new Cotizacion($data));
        return response()->json(['msg'=>'Mensaje enviado']);
    }
    public function agregarBoletin(Request $request)
    {
        $boletin = Boletin::where('correo',$request->correo)->first();
        if($boletin)
            return 0;

        $boletin = new Boletin();
        $boletin->correo = $request->correo;
        $boletin->nombre = $request->nombre;
        $boletin->save();

        $inputs = array('nombre' => $request->nombre);

        Correo::to($request->correo)->send(new NuevoBoletin($inputs));

        return 1;
    }
    public function agregarRifa(Request $request)
    {
        $rifa = Rifa::where('codigo',$request->codigo)->first();
        if($rifa)
            return 0;

        $rifa = new Rifa();
        $rifa->codigo = $request->codigo;
        $rifa->telefono = $request->telefono;
        $rifa->correo = $request->correo;
        $rifa->nombre = $request->nombre;
        $rifa->save();

        $inputs = array('nombre' => $request->nombre);

        Correo::to($request->correo)->send(new NuevaRifa($inputs));

        return 1;
    }
    public function boletin(){
        $usuarios = Boletin::all();
        $productos = Product::orderBy('created_at','DESC')->take(5)->get();

        foreach ($productos as $key => $value) {
            $value->image = Images::getUrl($value->image1_id);
            $value->subcategoria = SubCategory::find($value->subcategory_id)->name;
            $value->categoria = Category::find($value->category_id)->name;
        }

        foreach ($usuarios as $key => $value)
        {
           $inputs = array('nombre'=>$value->nombre, 'productos' => $productos);
           Correo::to($value->correo)->send(new Boletines($inputs));
        }
    }
}
