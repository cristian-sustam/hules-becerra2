<?php

namespace App\Http\Controllers;

use App\Application;
use Illuminate\Http\Request;
use App\Http\Requests\ApplicationsFormRequest;

class ApplicationsController extends Controller
{
    public function index($id)
    {
    	$apps = Application::where('product_id',$id)->get();
    	$data = [];
    	foreach ($apps as $key => $value) {
    		$value->brand;
    		$value->model;
    		array_push($data, $value);
    	}
    	return response()->json($data);
    }
    public function show($id)
    {
    	$app = Application::find($id);
    	$app->brand;
    	$app->model;
    	return response()->json($app);
    }
    public function showMotors($id)
    {
        $motors = Application::select('motor','model_id')->where('model_id',$id)->orderBy('motor','asc')->distinct()->get(['motor']);//Application::where('model_id',$id)->groupBy('motor')->get();
        $m = [];
        foreach ($motors as $key => $value)
        {
            $a = Application::select('id','motor','model_id')->where('model_id',$id)->where('motor',$value->motor)->first();
            array_push($m, $a);
        }
        return response()->json($m);
    }
    public function showYears($id)
    {
        $a = Application::find($id);
        $years = Application::where('brand_id',$a->brand_id)->where('model_id',$a->model_id)->where('motor',$a->motor)->get();

        $min = $max = 0;
        foreach ($years as $key => $value)
        {
            if($key == 0)
            {
                $min = $value->year_min;
                $max = $value->year_max;
            }
            else
            {
                if($value->year_min < $min)
                    $min = $value->year_min;

                if($value->year_max > $max)
                    $max = $value->year_max;
            }
        }
        $data = [];
        for($i = $min; $i<$max; $i++)
        {   
            $aux = ['year' => $i];
            array_push($data, $aux);
        }
        return response()->json($data);
    }
    public function store(ApplicationsFormRequest $request)
    {
    	$app = new Application();
    	$app->product_id = $request->product_id;
    	$app->brand_id = $request->brand_id['value'];
    	$app->model_id = $request->model_id['value'];
    	//$app->specifications = $request->specifications;
    	$app->motor = $request->motor;
    	$app->year_min = $request->year_min;
    	$app->year_max = $request->year_max;
    	$app->save();
    	return response()->json($app);
    }
    public function update(ApplicationsFormRequest $request,$id)
    {
    	$app = Application::find($id);
    	$app->product_id = $request->product_id;
    	$app->brand_id = $request->brand_id['value'];
    	$app->model_id = $request->model_id['value'];
    	//$app->specifications = $request->specifications;
    	$app->motor = $request->motor;
    	$app->year_min = $request->year_min;
    	$app->year_max = $request->year_max;
    	$app->save();
    	return response()->json($app);
    }
    public function destroy($id)
  	{
    	if($this->_deleteApplication($id))
      	return response()->json(['msg'=>'Aplicacion con ID '.$id.' eliminada.']);
    	else
      	return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
  	}
  	public function destroyMultiple(Request $request)
  	{
    	foreach ($request->ids as $key => $value)
    	{
      		$status=$this->_deleteApplication($value);
      		if(!$status)
        		break;
    	}
    	if ($status) 
      		return response()->json(['msg'=>'Aplicaciones eleminadas']);
    	else
      		return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
  	}
    private function _deleteApplication($app_id)
	{
		$app = Application::find($app_id);
	    if ($app->delete()) 
	      return true;
	    else
	      return false;
	  }
}
