<?php

namespace App\Http\Controllers;

use App\Carrusel;
use App\CarruselHasProductos;
use App\Product;
use Images;
use Illuminate\Http\Request;
//Para saber si las imagenes existen o no.
use Illuminate\Support\Facades\Storage;

class CarruselesController extends Controller
{ 
    public function home()
    {
      $carruseles = Carrusel::where('visible',1)->orderBy('posicion','ASC')->get();
      foreach ($carruseles as $key_ca => $ca)
      {
        $ca->productos();
        foreach ($ca->productos as $key_pro => $pro)
        {
          $pro->subcategory;
          if($pro->image1_id)
          {
            if(Storage::disk('public')->exists(Images::get($pro->image1_id)->path))
              $pro->imageUrl = Images::getUrl($pro->image1_id);
            else
              $pro->imageUrl = Images::getUrl(5);
          }
          else
            $pro->imageUrl = Images::getUrl(5);
        }
      }
      return response()->json($carruseles);
    }
    public function index()
    {
      $ca = Carrusel::all();
      foreach ($ca as $key => $value)
        $value->visible = ($value->visible)?("Si"):("No");
      return response()->json($ca);
    }
    public function show($id)
    {
      $ca = Carrusel::find($id);
      $ca->productos();
      return response()->json($ca);
    }
    public function store(Request $request)
    {
      $ca = new Carrusel();
      $ca->visible = ($request->visible)?(1):(0);
      $ca->nombre = $request->nombre;
      $ca->elementos =  $request->elementos;
      $ca->posicion = $request->posicion;
	  $ca->save();

      $pro = json_decode($request->productos,true);

      foreach ($pro as $key => $value)
      {
        $p = new CarruselHasProductos();
        $p->carrusel_id = $ca->id;
        $p->product_id = $value['value'];
        $p->save();
      }
      return response()->json($ca);
    }
    public function update(Request $request, $id)
    {
      $ca = Carrusel::find($id);
      $ca->visible = ($request->visible)?(1):(0);
      $ca->nombre = $request->nombre;
      $ca->elementos =  $request->elementos;
      $ca->posicion = $request->posicion;

      CarruselHasProductos::where('carrusel_id',$ca->id)->delete();

      $pro = json_decode($request->productos,true);

      foreach ($pro as $key => $value)
      {
        $p = new CarruselHasProductos();
        $p->carrusel_id = $ca->id;
        $p->product_id = $value['value'];
        $p->save();
      }

      $ca->save();
      return response()->json($ca->id);
    }
    
    public function destroy($id)
     {
       if($this->_deleteCarrusel($id))
           return response()->json(['msg'=>'Carrusel con ID '.$id.' eliminado.']);
       else
           return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     public function destroyMultiple(Request $request)
     {
        foreach ($request->ids as $key => $value)
        {
            $status = $this->_deleteCarrusel($value);
            if(!$status)
                break;
        }
        if ($status) 
            return response()->json(['msg'=>'Carruseles eliminados.']);
        else
            return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     private function _deleteCarrusel($carrusel_id)
     {
       $carrusel = Carrusel::find($carrusel_id);
       CarruselHasProductos::where('carrusel_id',$carrusel->id)->delete();
       if ($carrusel->delete())
           return true;
       else
           return false;
     }
}
