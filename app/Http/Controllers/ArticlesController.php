<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests\ArticlesFormRequest;
use Illuminate\Http\Request;
use Images;

class ArticlesController extends Controller
{
    public function index()
    {
      $articles = Article::all();
      $data=[];
      foreach ($articles as $key => $value) {
          $value->image=Images::getImg($value->image_id);
          array_push($data, $value);
      }
      return response()->json($data);
    }
    public function indexBlog()
    {
      $articles = Article::all();
      $data=[];
      foreach ($articles as $key => $value){
          $value->imageUrl=Images::getUrl($value->image_id);
          array_push($data, $value);
      }
      return response()->json($data);
    }
    public function show($id)
    {
      $article = Article::find($id);
      $article->imageUrl = Images::getUrl($article->image_id);
      return response()->json($article);
    }
    public function store(ArticlesFormRequest $request)
    {
      $article = new Article();
      $article->name = $request->name;
      $article->text = $request->text;
      if($request->image){
          $image_id=Images::save($request->image);
          $article->image_id=$image_id;
      }
      $article->save();
      $article->img;
      return response()->json($article);
    }
    public function update(ArticlesFormRequest $request, $id)
    {
      $article = Article::find($id);
      $article->name=$request->name;
      $article->text=$request->text;
      if(isset($request->image)){
          if ($article->image_id!=1) {
              //Borramos la imagen anterior
              Images::delete($article->image_id);
          }
          //Subimos la nueva imagen
          $image_id=Images::save($request->image);
          $article->image_id = $image_id;
      }

      $article->save();
      $article->img;
      return response()->json($article->id);
    }
    public function destroy($id)
     {
       if($this->_deleteArticle($id))
           return response()->json(['msg'=>'Articulo con ID '.$id.' eliminado.']);
       else
           return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     public function destroyMultiple(Request $request)
     {
         foreach ($request->ids as $key => $value)
         {
             $status=$this->_deleteArticle($value);
             if(!$status)
                 break;
         }
         if ($status) 
             return response()->json(['msg'=>'Articulos eliminados.']);
         else
             return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     private function _deleteArticle($article_id)
     {
       $article = Article::find($article_id);
       if($article->image_id != 1)
           Images::delete($article->image_id);
       if ($article->delete())
           return true;
       else
           return false;
     }
}
