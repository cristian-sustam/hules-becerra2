<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Boletin;

class BoletinesController extends Controller
{
    public function index() //Este es para el admin
    {
      $boletines = Boletin::all();
      return response()->json($boletines);
    }

    public function show($id)
    {
      $boletin = Boletin::find($id);
      return response()->json($boletin);
    }
    public function store(Request $request)
    {
    	$boletin = Boletin::where('correo',$request->correo)->first();
    	if($boletin)
    		return response()->json(['code' => -1]);

      	$boletin = new Boletin();
      	$boletin->nombre = $request->nombre;
      	$boletin->correo = $request->correo;
      	$boletin->save();
      	return response()->json(['code' => 1, 'boletin' => $boletin]);
    }
    public function update(Request $request, $id)
    {
		$boletin = Boletin::where('correo',$request->correo)->first();
		if($boletin)
			if($boletin->id != $id)
				return -1;

      	$boletin = Boletin::find($id);
 		$boletin->nombre = $request->nombre;
 		$boletin->correo = $request->correo;
      	$boletin->save();
      	return 1;
    }
    public function destroy($id)
     {
       if($this->_deleteBoletin($id)){
           return response()->json(['msg'=>'Boletin con ID '.$id.' eliminado.']);
       }
       else{
           return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
       }
     }

     public function destroyMultiple(Request $request)
     {
         foreach ($request->ids as $key => $value) {
             $status=$this->_deleteBoletin($value);
             if(!$status)
                 break;
         }

         if ($status) {
             return response()->json(['msg'=>'Boletines eliminados.']);
         }
         else{
             return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
         }
     }

     private function _deleteBoletin($boletin_id)
     {
       $boletin = Boletin::find($boletin_id);

       if ($boletin->delete()) {
           return true;
       }
       else{
           return false;
       }
     }
}
