<?php

namespace App\Http\Controllers;
use App\SubCategory;
use App\Category;
use Illuminate\Http\Request;

class SubCategoriesController extends Controller
{
    public function index()
    {
      $subs=SubCategory::all();
      foreach ($subs as $key => $value)
      {
        $value->category;
        $value->available = ($value->available)?("Si"):("No");
      }
      return response()->json($subs);
    }
    public function indexAlfabeticamente()
    {
      $subs=SubCategory::where('available',true)->orderBy('name','ASC')->get();
      foreach ($subs as $key => $value)
        $value->category;
      return response()->json($subs);
    }
    public function indexAlfabeticamenteOtros()
    {
      $subs=SubCategory::where('available',false)->orderBy('name','ASC')->get();
      foreach ($subs as $key => $value)
        $value->category;
      return response()->json($subs);
    }
    public function show($id)
    {
      $sub=SubCategory::find($id);
      $sub->category;
      return response()->json($sub);
    }
    public function showCategory($id)
    {
      $subs=SubCategory::where('category_id',$id)->get();
      return response()->json($subs);
    }
    public function categoryShow($category)
    { 
      $subs=[];
      $cat = str_replace(array("_"), " ", $category);
      $cat = Category::where('name',$cat)->first();
      if($cat)
        $subs=SubCategory::where('category_id',$cat->id)->get();
      return response()->json($subs);
    }
    public function store(Request $request)
    {
      $name = SubCategory::where('name',$request->name)->first();
      if($name)
      	if($name->category_id == $request->category_id['value'])
      		return response()->json(['msg'=>'El nombre de la subcategoria ya existe para esa categoria']);

      $sub = new SubCategory();
      $sub->available = ($request->available)?(1):(0);
      $sub->name = $request->name;
      $sub->category_id = $request->category_id['value'];
      $sub->save();
      return response()->json($sub);
    }
    public function update(Request $request, $id)
    {
      $name = SubCategory::where('name',$request->name)->first();
      if($name)
      	if($name->id!=$id)
      		return response()->json(['msg'=>'El nombre de la subcategoria ya existe']);

      $sub=SubCategory::find($id);
      
      $sub->available = ($request->available)?(1):(0);
      $sub->name = $request->name;
      $sub->category_id = $request->category_id['value'];
      $sub->save();
      return response()->json($sub->id);
    }
    public function destroy($id)
     {
       if($this->_deleteSubCategoria($id))
           return response()->json(['msg'=>'Subcategoria con ID '.$id.' eliminada.']);
       else
           return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     public function destroyMultiple(Request $request)
     {
         foreach ($request->ids as $key => $value)
         {
             $status=$this->_deleteSubCategoria($value);
             if(!$status)
                 break;
         }

         if ($status) 
             return response()->json(['msg'=>'Subcategorias eliminadas.']);
         else
             return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
     }

     private function _deleteSubCategoria($sub_id)
     {
       $sub=SubCategory::find($sub_id);
       if ($sub->delete())
           return true;
       else
           return false;
     }
}
