<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\ProductsFormRequest;
use Images;
use App\Application;
use App\Image;

//Para saber si las imagenes existen o no.
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
  public function index() //listo
  {
    $products=Product::orderBy('position','asc')->get();
    $data=[];
    foreach ($products as $key => $value) {

      if($value->image1_id)
      {
        if(Storage::disk('public')->exists(Images::get($value->image1_id)->path))
          $value->image1 = Images::getImg($value->image1_id);
        else
          $value->image1 = Images::getImg(5);
      }
      else
        $value->image1 = Images::getImg(5);
      //-------------------------------------------------------------------------------------
      if($value->image2_id)
      {
        if(Storage::disk('public')->exists(Images::get($value->image2_id)->path))
          $value->image2 = Images::getImg($value->image2_id);
        else
          $value->image2 = Images::getImg(5);
      }
      else
        $value->image2 = Images::getImg(5);
      //-------------------------------------------------------------------------------------
      if($value->image3_id)
      {
        if(Storage::disk('public')->exists(Images::get($value->image3_id)->path))
          $value->image3 = Images::getImg($value->image3_id);
        else
          $value->image3 = Images::getImg(5);
      }
      else
        $value->image3 = Images::getImg(5);
      //-------------------------------------------------------------------------------------
      if($value->image4_id)
      {
        if(Storage::disk('public')->exists(Images::get($value->image4_id)->path))
          $value->image4 = Images::getImg($value->image4_id);
        else
          $value->image4 = Images::getImg(5);
      }
      else
        $value->image4 = Images::getImg(5);

        $value->category;
        $value->subcategory;
        array_push($data, $value);
    }
    return response()->json($data);
  }
  public function store(ProductsFormRequest $request) //listo
  {
    if(Product::where('sku',$request->sku)->first())
      return response()->json(['msg'=>'El producto con el sku '.$request->sku.' ya existe']);
    $product = new Product();
    $product->sku = $request->sku;
    $product->name = $request->name;
    $product->equivalence1 = $request->equivalence1;
    $product->equivalence2 = $request->equivalence2;
    $product->equivalence3 = $request->equivalence3;
    $product->category_id = $request->category_id['value'];
    $product->subcategory_id = $request->subcategory_id['value'];
    $product->specifications = $request->specifications;
    $product->position = $request->position;
    if($request->stock != "")
      $product->stock = $request->stock;

    if($request->image1)
    {
        $image1_id=Images::save($request->image1);
        $product->image1_id=$image1_id;
    }
    else
    {
      $image = new Image();
      $image->path = "photos/". $product->sku ."a.jpg";
      $image->disk = "public";
      $image->key = "".$product->sku."a";
      $image->save();
      $product->image1_id = $image->id;
    }
    if($request->image2)
    {
        $image2_id=Images::save($request->image2);
        $product->image2_id=$image2_id;
    }
    else
    {
      $image = new Image();
      $image->path = "photos/". $product->sku ."b.jpg";
      $image->disk = "public";
      $image->key = "".$product->sku."b";
      $image->save();
      $product->image2_id = $image->id;
    }
    if($request->image3)
    {
        $image3_id=Images::save($request->image3);
        $product->image3_id=$image3_id;
    }
    else
    {
      $image = new Image();
      $image->path = "photos/". $product->sku ."c.jpg";
      $image->disk = "public";
      $image->key = "".$product->sku."c";
      $image->save();
      $product->image3_id = $image->id;
    }
    if($request->image4)
    {
        $image4_id=Images::save($request->image4);
        $product->image4_id=$image4_id;
    }
    else
    {
      $image = new Image();
      $image->path = "photos/". $product->sku ."d.jpg";
      $image->disk = "public";
      $image->key = "".$product->sku."d";
      $image->save();
      $product->image4_id = $image->id;
    }
    $product->save();
    return response()->json($product);
  }
  public function show($id) //listo
  {
    $product=Product::find($id);

    if($product->image1_id)
    {
      if(Storage::disk('public')->exists(Images::get($product->image1_id)->path))
        $product->image1Url = Images::getUrl($product->image1_id);
      else
        $product->image1Url = Images::getUrl(5);
    }
    else
      $product->image1Url = Images::getUrl(5);

    if($product->image2_id)
    {
      if(Storage::disk('public')->exists(Images::get($product->image2_id)->path))
        $product->image2Url = Images::getUrl($product->image2_id);
      else
        $product->image2Url = Images::getUrl(5);
    }
    else
      $product->image2Url = Images::getUrl(5);

    if($product->image3_id)
    {
      if(Storage::disk('public')->exists(Images::get($product->image3_id)->path))
        $product->image3Url = Images::getUrl($product->image3_id);
      else
        $product->image3Url = Images::getUrl(5);
    }
    else
      $product->image3Url = Images::getUrl(5);

    if($product->image4_id)
    {
      if(Storage::disk('public')->exists(Images::get($product->image4_id)->path))
        $product->image4Url = Images::getUrl($product->image4_id);
      else
        $product->image4Url = Images::getUrl(5);
    }
    else
      $product->image4Url = Images::getUrl(5);

    $product->subcategory;
    $product->category;
    return response()->json($product);
  }
  public function update(ProductsFormRequest $request, $id) //listo
  {
    $product = Product::where('sku',$request->sku)->first();
    if($product)
    {
      if($product->id != $id)
        return response()->json(['msg'=>'El producto con el sku '.$request->sku.' ya existe']);
    }
    else
      $product = Product::find($id);

    $product->name = $request->name;
    $product->sku = $request->sku;
    $product->equivalence1 = $request->equivalence1;
    $product->equivalence2 = $request->equivalence2;
    $product->equivalence3 = $request->equivalence3;
    $product->category_id = $request->category_id['value'];
    $product->subcategory_id = $request->subcategory_id['value'];    
    $product->specifications = $request->specifications;
    $product->position = $request->position;
    if($request->stock != "")
      $product->stock = $request->stock;

    if(isset($request->image1))
    {
      if ($product->image1_id!=5) 
        Images::delete($product->image1_id);
      $image1_id=Images::save($request->image1);
      $product->image1_id=$image1_id;
    }
    if(isset($request->image2))
    {
      if ($product->image2_id!=5)
        Images::delete($product->image2_id);
      $image2_id=Images::save($request->image2);
      $product->image2_id=$image2_id;
    }
    if(isset($request->image3))
    {
      if ($product->image3_id!=5) 
        Images::delete($product->image3_id);
      $image3_id=Images::save($request->image3);
      $product->image3_id=$image3_id;
    }
    if(isset($request->image4))
    {
      if ($product->image4_id!=5)
        Images::delete($product->image4_id);
      $image4_id=Images::save($request->image4);
      $product->image4_id=$image4_id;
    }
    $product->save();
    return response()->json($product->id);
  }
  public function destroy($id)
  {
    if($this->_deleteProduct($id))
      return response()->json(['msg'=>'Producto con ID '.$id.' eliminado.']);
    else
      return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
  }
  public function destroyMultiple(Request $request)
  {
    foreach ($request->ids as $key => $value)
    {
      $status=$this->_deleteProduct($value);
      if(!$status)
        break;
    }
    if ($status) 
      return response()->json(['msg'=>'Productos eleminados']);
    else
      return response()->json(['msg'=>'Ocurrio un error al eliminar.'],500);
  }
  private function _deleteProduct($product_id)
  {
    $product= Product::find($product_id);
    if($product->image1_id != 5)
      Images::delete($product->image1_id);
    if($product->image2_id != 5)
      Images::delete($product->image2_id);
    if($product->image3_id != 5)
      Images::delete($product->image3_id);
    if($product->image4_id != 5)
      Images::delete($product->image4_id);

      $apps = Application::where('product_id',$product->id)->get();
      foreach ($apps as $key => $value)
      {
        $value->delete();
      }
      
    if ($product->delete()) 
      return true;
    else
      return false;
  }

  public function migrarImagenes()
  {
    $archivos = Storage::disk('public')->files('/images');
    foreach ($archivos as $key => $archivo)
    {
      $nombre_original = substr($archivo,7);
      $nombre = str_replace([".jpg",".JPG"], null, $nombre_original);

      $producto = Product::where('sku',$nombre)->first();
      if($producto)
      {
        if($producto->image1_id != 1 && $producto->image1_id != 5)
        {
          $image = Image::find($producto->image1_id);

          if(Storage::disk('public')->exists($image->path)) //si existe la foto la borramos
            Storage::disk('public')->delete($image->path);

          Storage::disk('public')->copy('images/'.$nombre_original, 'photos/'.$nombre_original);
          $image->path = "photos/".$nombre_original;
          $image->key = $nombre;
          $image->save();
          echo "Imagen de producto->".$nombre." guardada \n";
        }
      }
      else
        echo "No se encontro el producto->".$nombre."\n";

    }
    return 1;
  }
}
