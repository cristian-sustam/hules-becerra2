<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria para utilizar el borrado lógico

class Models extends Model
{
  use SoftDeletes; //Implementamos -> para el borrado lógico
  protected $dates = ['deleted_at']; //Registramos la nueva columna -> para el borrado lógico
  
  protected $fillable = ['name','image_id','brand_id'];

  public function brand(){
    return $this->belongsTo('App\Brand','brand_id','id')->withTrashed();
  }
}
