<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria para utilizar el borrado lógico

class Brand extends Model
{
    use SoftDeletes; //Implementamos -> para el borrado lógico
  	protected $dates = ['deleted_at']; //Registramos la nueva columna -> para el borrado lógico
}
