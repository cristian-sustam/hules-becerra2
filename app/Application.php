<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
  protected $fillable = ['id','product_id','brand_id','model_id','motor','year_min','year_max','specifications'];

  public function brand(){
    return $this->belongsTo('App\Brand','brand_id','id')->withTrashed();
  }
  public function model(){
    return $this->belongsTo('App\Models','model_id','id')->withTrashed();
  }
}
