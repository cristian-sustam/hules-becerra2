<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = ['sku','image1_id','image2_id','image3_id','image4_id','brand_id','model_id','years_rank','specifications','motor','subcategory_id','category_id','stock','position'];

  public function brand(){
    return $this->belongsTo('App\Brand','brand_id','id');
  }
  public function model(){
    return $this->belongsTo('App\Models','model_id','id');
  }
  public function subcategory(){
    return $this->belongsTo('App\SubCategory','subcategory_id','id');
  }
  public function category(){
    return $this->belongsTo('App\Category','category_id','id');
  }
   public function apps(){
    return $this->hasMany('App\Application','product_id','id');
  }
}
