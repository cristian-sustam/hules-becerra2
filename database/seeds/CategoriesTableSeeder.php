<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            "id"=>1,
            "name"=>"Automotriz"
        ]);
        DB::table('categories')->insert([
            "id"=>2,
            "name"=>"Industrial"
        ]);
        DB::table('categories')->insert([
            "id"=>3,
            "name"=>"Equipo Pesado"
        ]);
        DB::table('categories')->insert([
            "id"=>4,
            "name"=>"Otros"
        ]);
        DB::table('categories')->insert([
            "id"=>5,
            "name"=>"Grapa automotriz"
        ]);
        DB::table('categories')->insert([
            "id"=>6,
            "name"=>"Universal"
        ]);
        
    }
}
