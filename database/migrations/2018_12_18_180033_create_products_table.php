<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku');
            $table->string('name')->nullable();
            $table->integer('image1_id')->default(5);
            $table->integer('image2_id')->default(5);
            $table->integer('image3_id')->default(5);
            $table->integer('image4_id')->default(5);
            $table->string('equivalence1')->nullable();
            $table->string('equivalence2')->nullable();
            $table->string('equivalence3')->nullable();
            $table->integer('category_id');
            $table->integer('subcategory_id');
            $table->integer('stock')->nullable();                  
            $table->longText('specifications')->nullable();
            $table->integer('position')->default(1);
            $table->timestamps();
        });
        /*Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku');
            $table->string('name')->nullable();
            $table->integer('image1_id')->default(1);
            $table->integer('image2_id')->default(1);
            $table->integer('image3_id')->default(1);
            $table->integer('image4_id')->default(1);
            $table->integer('brand_id');
            $table->integer('model_id');
            $table->integer('year_min');
            $table->integer('year_max');
            $table->string('equivalence1')->nullable();
            $table->string('equivalence2')->nullable();
            $table->string('equivalence3')->nullable();
            $table->string('specifications')->nullable();
            $table->string('motor')->nullable();

            $table->integer('category_id');
            $table->integer('subcategory_id');
            $table->integer('stock');
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
