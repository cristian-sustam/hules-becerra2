<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content ="{!!csrf_token()!!}" />

	<link rel="icon" type="image/png"   href="public/images/shared/favicon.png">

	<title>Cargando..</title>

	<link rel="stylesheet" type="text/css" href="public/css/app.css">

	<link rel="preload" href="public/extras/css/font-awesome/css/all.min.css" >
	<script src="public/extras/css/font-awesome/js/all.min.js"></script>

	<!--<script src="https://www.paypalobjects.com/api/checkout.js"></script>-->
	@include('shared.jsDir')
</head>
<body>
	<div id="app">
		<vue-topprogress ref="loadingBar" color="#2bb86c" :height="4"></vue-topprogress>
		<my-header></my-header>
		<header-relleno></header-relleno>
		<router-view></router-view>
		<my-footer></my-footer>
	</div>

	<a class="whatsapp-bigicon" target="_blank" href="https://web.whatsapp.com/send?phone=523317283818">
    <i class="fab fa-whatsapp w-icon"></i>
  </a>
	
	<script type="text/javascript" src="public/js/app.js"></script>
</body>
</html>
