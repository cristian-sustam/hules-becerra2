@component('mail::message')
# Hola {{$inputs['nombre']}}, tu solicitud para participar en la rifa fue enviada!.
Mantente al pendiente para que puedas reclamar tu premio en caso de resultar ganador. Mucha suerte!
@endcomponent