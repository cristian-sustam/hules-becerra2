@component('mail::message')
# Garantia
El cliente: {{$inputs['nombre']}}, ha solicitado una garantia.<br>
No. de factura o ticket: {{$inputs['codigo']}}<br>
Código de producto: {{$inputs['codigo']}}<br>
Descripcion del problema: {{$inputs['problema']}}<br>
Comentarios adicionales: {{$inputs['comentario']}}<br>

@endcomponent