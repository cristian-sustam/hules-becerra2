@component('mail::message')
#Contacto

Un visitante con el nombre: "{{$inputs['name']}}" ha dejado un mensaje"<br>
"{{$inputs['text']}}".<br>
El correo de contacto que proporcionó: {{$inputs['email']}}

@endcomponent
