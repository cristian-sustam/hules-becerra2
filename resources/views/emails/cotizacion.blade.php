@component('mail::message')
# Cotización
El cliente: {{$inputs['nombre']}}, ha solicitado una cotización.<br>
Su correo: {{$inputs['correo']}} <br>
Su telefono: {{$inputs['telefono']}} <br><br>

Los productos que cotizó, son los siguientes.

@component('mail::table')
|Imagen|Sku|Subcategoria|Cantidad      
|:-:|:-:|:-:|:-:|
@foreach($inputs['productos'] as $producto)
|<img src="{{$producto['image']}}" width="50px" height="50px">|{{$producto['sku']}}|{{$producto['subcategory']}}|{{$producto['cantidad']}}|
@endforeach

Este es un mensaje automático hecho por internetizante.com/hules-becerra

@endcomponent
@endcomponent
