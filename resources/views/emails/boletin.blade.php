@component('mail::message')

#Hola {{$inputs['nombre']}}, aquí tienes una muestra de nuestros productos mas recientes!<br>

@component('mail::table')
|Imagen|Sku|Subcategoria|Categoria|      
|:-:|:-:|:-:|:-:|
@foreach($inputs['productos'] as $producto)
|<img src="{{$producto['image']}}" width="50px" height="50px">|{{$producto['sku']}}|{{$producto['subcategoria']}}|{{$producto['categoria']}}|
@endforeach


@endcomponent
@endcomponent
