import Vue from 'vue';
import VueRouter from 'vue-router';
import VueAnalytics from 'vue-analytics';


Vue.use(VueRouter);

//Componentes
//import Login from './components/admin/Login.vue';

const page="./components/page/";

const MyRouter = new VueRouter({
  	routes:[
      { path: '/', component: require(page+'home.vue'), meta:{title:"Home"}},
      { path: '/contacto', component: require(page+'contacto/index.vue'), meta:{title:"Contacto"}},
      { path: '/empresa', component: require(page+'empresa/index.vue'), meta:{title:"Empresa"}},
	  { path: '/servicios', component: require(page+'servicios/index.vue'), meta:{title:"Servicios"}},
	  { path: '/blog', component: require(page+'blog/index.vue'), meta:{title:"Blog"}},
	  { path: '/preguntas', component: require(page+'preguntas/index.vue'), meta:{title:"Preguntas Frecuentes"}},
	  { path: '/garantia', component: require(page+'garantias/index.vue'), meta:{title:"Garantias"}},
	  { path: '/rifa', component: require(page+'rifa/index.vue'), meta:{title:"Rifa"}},

      { path: '/privacidad', component: require(page+'legal/politicas-de-privacidad.vue'), meta:{title:"Politicas de Privacidad"}},

      { path: '/cotizacion', component: require(page+'cotizar/index.vue'), meta:{title:"Cotización"}},

      { path: '/productos/automotriz', component: require(page+'productos/automotriz.vue'), meta:{title:"Productos automotriz"}},
      { path: '/productos/industrial', component: require(page+'productos/industrial.vue'), meta:{title:"Productos industrial"}},
      { path: '/productos/equipo_pesado', component: require(page+'productos/equipo_pesado.vue'), meta:{title:"Productos equipo pesado"}},
      { path: '/productos/grapa_automotriz', component: require(page+'productos/grapa_automotriz.vue'), meta:{title:"Productos grapa automotriz"}},
      { path: '/productos/universal', component: require(page+'productos/universal.vue'), meta:{title:"Productos Universales"}},
      { path: '/productos/empaque', component: require(page+'productos/empaque.vue'), meta:{title:"Productos empaque"}},


      { path: '/productos', component: require(page+'productos/index.vue'), meta:{title:"Productos"}},
      { path: '/productos/detalle/:id', component: require(page+'productos/detalle.vue'), meta:{title:"Productos"}},
	    // { path: '/checkout', component: require(page+'checkout.vue'), meta:{title:"Checkout"}},
	  ]
});

Vue.use(VueAnalytics, {
  id: 'UA-136675726-1',
  MyRouter
});

MyRouter.beforeEach((to, from, next) => {
	window.scrollTo(0,0);
	if(window.app.__vue__ && window.app.__vue__.$refs.loadingBar){
		window.app.__vue__.$refs.loadingBar.start();
	}
	next();
});

MyRouter.afterEach((to, from) => {

	if(window.app.__vue__ && window.app.__vue__.$refs.loadingBar){
		setTimeout(()=>{
			window.app.__vue__.$refs.loadingBar.done();
		},500);
	}


});

//Titulos del website
import VueDocumentTitlePlugin from "vue-document-title-plugin";
Vue.use(VueDocumentTitlePlugin, MyRouter,
	{ defTitle: "Home", filter: (title)=>{ return title+" - Hules Automotrices Becerra"; } }
);

// export {routes};
export default MyRouter;
