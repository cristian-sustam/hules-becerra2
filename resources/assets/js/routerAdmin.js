import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const page="./components/admin/";

const MyRouter = new VueRouter({
  	routes:[
	    { path: '/', redirect:"/login"},
	    { path: '/login', component: require(page+'login.vue'), meta:{title:"Login"}},
	    { path: '/home', component: require(page+'home.vue'), meta:{title:"Home"}},
	    { path: '/profile', component: require(page+'me.vue'), meta:{title:"Mi perfil"}},

	    //Insercion a la base desde archivo de excel
	    { path: '/insertionsAutomotriz', component: require(page+'insertions/automotriz.vue'), meta:{title:"Insercion automotriz"}},
      { path: '/insertionsOtros', component: require(page+'insertions/otros.vue'), meta:{title:"Insercion otros"}},
	    //Usuarios
	    { path: '/users', component: require(page+'users/index.vue'), meta:{title:"Usuarios"}},
	    { path: '/users/edit', component: require(page+'users/edit.vue'), meta:{title:"Editar"}},//Cuando no envian parametro
	    { path: '/users/edit/:id', component: require(page+'users/edit.vue'), meta:{title:"Editar"}},//Con parametro
	    //Roles
	    { path: '/roles', component: require(page+'configuration/roles.vue'), meta:{title:"Roles"}},
	    { path: '/roles/edit/:id', component: require(page+'configuration/permissions.vue'), meta:{title:"Editar"}},
     //Marcas
      { path: '/brands', component: require(page+'brands/index.vue'), meta:{title:"Marcas"}},
      { path: '/brands/edit', component: require(page+'brands/edit.vue'), meta:{title:"Editar"}},
      { path: '/brands/edit/:id', component: require(page+'brands/edit.vue'), meta:{title:"Editar"}},

      //Boletines
      { path: '/boletines', component: require(page+'boletines/index.vue'), meta:{title:"Boletines"}},
      { path: '/boletines/edit', component: require(page+'boletines/edit.vue'), meta:{title:"Editar"}},
      { path: '/boletines/edit/:id', component: require(page+'boletines/edit.vue'), meta:{title:"Editar"}},

      //Rifas
      { path: '/rifas', component: require(page+'rifas/index.vue'), meta:{title:"Rifas"}},
      { path: '/rifas/edit', component: require(page+'rifas/edit.vue'), meta:{title:"Editar"}},
      { path: '/rifas/edit/:id', component: require(page+'rifas/edit.vue'), meta:{title:"Editar"}},

    //Productos
      { path: '/products', component: require(page+'products/index.vue'), meta:{title:"Products"}},
      { path: '/products/edit', component: require(page+'products/edit.vue'), meta:{title:"Editar"}},
      { path: '/products/edit/:id', component: require(page+'products/edit.vue'), meta:{title:"Editar"}},

    //Aplicaciones
    { path: '/applications', component: require(page+'applications/index.vue'), meta:{title:"Aplicaciones"}},
    { path: '/applications/show/:id',component: require(page+'applications/show.vue'), meta:{title:"Aplicaciones"}},
    { path: '/applications/:id/edit',component: require(page+'applications/edit.vue'), meta:{title:"Aplicaciones"}},
    { path: '/applications/:id/edit/:id2',component: require(page+'applications/edit.vue'), meta:{title:"Aplicaciones"}},


	      /*//Automotriz
	      { path: '/products/automotive', component: require(page+'products/indexAutomotive.vue'), meta:{title:"Automotriz"}},
	      { path: '/products/automotive/edit', component: require(page+'products/editAutomotive.vue'), meta:{title:"Edit"}},
	      { path: '/products/automotive/edit/:id', component: require(page+'products/editAutomotive.vue'), meta:{title:"Edit"}},

	      //Industrial
	      { path: '/products/industrial', component: require(page+'products/indexIndustrial.vue'), meta:{title:"Industrial"}},
	      { path: '/products/industrial/edit', component: require(page+'products/editIndustrial.vue'), meta:{title:"Edit"}},
	      { path: '/products/industrial/edit/:id', component: require(page+'products/editIndustrial.vue'), meta:{title:"Edit"}},

	      //Equipo pesado
	      { path: '/products/heavy', component: require(page+'products/indexHeavy.vue'), meta:{title:"Equipo pesado"}},
	      { path: '/products/heavy/edit', component: require(page+'products/editHeavy.vue'), meta:{title:"Edit"}},
	      { path: '/products/heavy/edit/:id', component: require(page+'products/editHeavy.vue'), meta:{title:"Edit"}},*/

      

      //Banners
      { path: '/banners', component: require(page+'banners/index.vue'), meta:{title:"Banners"}},
      { path: '/banners/edit', component: require(page+'banners/edit.vue'), meta:{title:"Editar"}},
      { path: '/banners/edit/:id', component: require(page+'banners/edit.vue'), meta:{title:"Editar"}},

      //Articulos
      { path: '/articles', component: require(page+'articles/index.vue'), meta:{title:"Articulos"}},
      { path: '/articles/edit', component: require(page+'articles/edit.vue'), meta:{title:"Articulos"}},
      { path: '/articles/edit/:id', component: require(page+'articles/edit.vue'), meta:{title:"Articulos"}},

     //Productos
      { path: '/models', component: require(page+'models/index.vue'), meta:{title:"Models"}},
      { path: '/models/edit', component: require(page+'models/edit.vue'), meta:{title:"Editar"}},
      { path: '/models/edit/:id', component: require(page+'models/edit.vue'), meta:{title:"Editar"}},
	 
	  //Categorias
	  { path: '/categories', component: require(page+'categories/index.vue'), meta:{title:"Categories"}},
    { path: '/categories/edit/:id', component: require(page+'categories/edit.vue'), meta:{title:"Editar"}}, 
 
      //Subcategorias
	  { path: '/subcategories', component: require(page+'subcategories/index.vue'), meta:{title:"Subcategories"}},
    { path: '/subcategories/edit', component: require(page+'subcategories/edit.vue'), meta:{title:"Editar"}},
    { path: '/subcategories/edit/:id', component: require(page+'subcategories/edit.vue'), meta:{title:"Editar"}},

    //Carruseles
    { path: '/carruseles', component: require(page+'carruseles/index.vue'), meta:{title:"Carruseles"}},
    { path: '/carruseles/edit', component: require(page+'carruseles/edit.vue'), meta:{title:"Editar"}},
    { path: '/carruseles/edit/:id', component: require(page+'carruseles/edit.vue'), meta:{title:"Editar"}},

      ]
});

//Titulos del website
import VueDocumentTitlePlugin from "vue-document-title-plugin";
Vue.use(VueDocumentTitlePlugin, MyRouter, 
	{ defTitle: "Base laravel", filter: (title)=>{ return title+" - Base laravel"; } }
);
	  
// export {routes};
export default MyRouter;