<?php

use Illuminate\Http\Request;

require('basics.php');

Route::get('/front/products/automotive/{perPage}',"ProductsFrontController@automotiveIndex");
Route::get('/front/products/empaque/{perPage}',"ProductsFrontController@empaqueIndex");
Route::get('/front/products/heavy/{perPage}',"ProductsFrontController@heavyIndex");
Route::get('/front/products/industrial/{perPage}',"ProductsFrontController@industrialIndex");
Route::get('/front/products/grapa/{perPage}',"ProductsFrontController@grapaIndex");
Route::get('/front/products/universal/{perPage}',"ProductsFrontController@universalIndex");
Route::get('/front/products/otros/{perPage}',"ProductsFrontController@otrosIndex");

Route::get('/front/product/{id}',"ProductsFrontController@show");

Route::get('/front/brands',"BrandsController@indexAlfabeticamente");
Route::get('/front/models/{id}',"ModelsController@showBrand");
Route::get('/front/motors/{id}',"ApplicationsController@showMotors");
Route::get('/front/years/{id}',"ApplicationsController@showYears");
Route::get('/front/subcategories',"SubCategoriesController@indexAlfabeticamente");
Route::get('/front/subcategoriesOtros',"SubCategoriesController@indexAlfabeticamenteOtros");

Route::post('/front/advanceFilter/{perPage}',"ProductsFrontController@advanceFilter");
Route::post('/front/simpleFilter/{perPage}',"ProductsFrontController@simpleFilter");

Route::get('/front/banners',"BannersController@indexHome");
//Route::get('/front/promos',"ProductsFrontController@promos"); SE CAMBIO POR CARRUSELES
Route::get('/front/articles',"ArticlesController@indexBlog");

Route::post('/front/message',"MailController@contact");
Route::post('/front/warranty',"MailController@warranty");
Route::post('/front/cotizacion',"MailController@cotizacion");


Route::get('/front/addProduct/{id}',"ProductsFrontController@add");

Route::post('/front/boletin',"MailController@agregarBoletin");
Route::post('/front/rifa',"MailController@agregarRifa");

Route::get('/front/carruseles',"CarruselesController@home");

//Grupo de rutas que requieren autentificacion
Route::middleware(["jwt.auth"])->group(function(){
	//Rutas
});