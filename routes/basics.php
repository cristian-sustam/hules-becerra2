<?php

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Estas rutas no requieren autentificacion
Route::post('/login/',"AuthenticateController@authenticate");
Route::post('/logout/',"AuthenticateController@logout");

//Grupo de rutas que requieren autentificacion
Route::middleware(["jwt.auth"])->group(function(){
	//Estas rutas no requieren ninguna validacion
	Route::get('/me/',"AuthenticateController@me");			//Obtener informacion de la session
	Route::post('/me/',"AuthenticateController@update");	//Actualizacion de informacion de usuario
	Route::get('/menu/',"MenusController@index");			//Obtener menu de usuario
	//Insercion de archivo
	Route::post('/fileAutomotriz',"InsertionsController@insert")->middleware('auth.perm:edit_insertions');
	Route::post('/fileOtros',"InsertionsController@insertOtros")->middleware('auth.perm:edit_insertions');
	//Usuarios
	Route::get('/users/',"UsersController@index")->middleware('auth.perm:view_users');
	Route::get('/user/{id}',"UsersController@show")->middleware('auth.perm:view_users');
	Route::post('/user/',"UsersController@store")->middleware('auth.perm:edit_users');
	Route::post('/user/{id}',"UsersController@update")->middleware('auth.perm:edit_users');
	Route::delete('/user/{id}',"UsersController@destroy")->middleware('auth.perm:edit_users');
	Route::delete('/users/',"UsersController@destroyMultiple")->middleware('auth.perm:edit_users');

	//Edicion de roles
	Route::get('/permissions/','RolesController@permissions')->middleware('auth.role:administrador');
	Route::get('/roles/','RolesController@index')->middleware('auth.role:administrador');
	Route::get('/role/{id}','RolesController@show')->middleware('auth.role:administrador');
	Route::post('/role/','RolesController@store')->middleware('auth.role:administrador');
	Route::post('/role/{id}','RolesController@update')->middleware('auth.role:administrador');
	Route::delete('/role/{id}','RolesController@destroy')->middleware('auth.role:administrador');

	//Notificaciones
	Route::get('/notifications/{user_id}','NotificationController@index');
	Route::get('/notifications/{user_id}/resume','NotificationController@resume');
	Route::post('/notifications/{user_id}/read_all','NotificationController@readAll');
	Route::post('/notifications/create','NotificationController@store');

	//Conekta
	Route::post('/conekta/card',"ConektaController@add_credit_card");
	Route::delete('/conekta/card/{token}',"ConektaController@delete_credit_card");
	Route::post('/conekta/order',"ConektaController@create_order");
	Route::post('/conekta/order/refund/{token}',"ConektaController@refund_order");

	//Marcas
	Route::get('/brands/','BrandsController@index')->middleware('auth.perm:view_brands');
	Route::get('/brand/{id}','BrandsController@show')->middleware('auth.perm:view_brands');
	Route::post('/brand/','BrandsController@store')->middleware('auth.perm:edit_brands');
	Route::post('/brand/{id}','BrandsController@update')->middleware('auth.perm:edit_brands');
	Route::delete('/brand/{id}','BrandsController@destroy')->middleware('auth.perm:edit_brands');
	Route::delete('/brands/','BrandsController@destroyMultiple')->middleware('auth.perm:edit_brands');

	//Boletines
	Route::get('/boletines/','BoletinesController@index')->middleware('auth.perm:view_boletines');
	Route::get('/boletin/{id}','BoletinesController@show')->middleware('auth.perm:view_boletines');
	Route::post('/boletin/','BoletinesController@store')->middleware('auth.perm:edit_boletines');
	Route::post('/boletin/{id}','BoletinesController@update')->middleware('auth.perm:edit_boletines');
	Route::delete('/boletin/{id}','BoletinesController@destroy')->middleware('auth.perm:edit_boletines');
	Route::delete('/boletines/','BoletinesController@destroyMultiple')->middleware('auth.perm:edit_boletines');

	//Boletines
	Route::get('/rifas/','RifasController@index')->middleware('auth.perm:view_rifas');
	Route::get('/rifa/{id}','RifasController@show')->middleware('auth.perm:view_rifas');
	Route::post('/rifa/','RifasController@store')->middleware('auth.perm:edit_rifas');
	Route::post('/rifa/{id}','RifasController@update')->middleware('auth.perm:edit_rifas');
	Route::delete('/rifa/{id}','RifasController@destroy')->middleware('auth.perm:edit_rifas');
	Route::delete('/rifas/','RifasController@destroyMultiple')->middleware('auth.perm:edit_rifas');

	//Modelos
	Route::get('/models/','ModelsController@index')->middleware('auth.perm:view_models');
	Route::get('/models/brand/{id}','ModelsController@showBrand')->middleware('auth.perm:view_models');
	Route::get('/model/{id}','ModelsController@show')->middleware('auth.perm:view_models');
	Route::post('/model/','ModelsController@store')->middleware('auth.perm:edit_models');
	Route::post('/model/{id}','ModelsController@update')->middleware('auth.perm:edit_models');
	Route::delete('/model/{id}','ModelsController@destroy')->middleware('auth.perm:edit_models');
	Route::delete('/models/','ModelsController@destroyMultiple')->middleware('auth.perm:edit_models');


	//Productos
		//index
		Route::get('/productos/','ProductsController@index')->middleware('auth.perm:view_products');
		Route::delete('/productos/','ProductsController@destroyMultiple')->middleware('auth.perm:edit_products');
		//edit
		Route::post('/producto','ProductsController@store')->middleware('auth.perm:edit_products');
		Route::post('/producto/{id}','ProductsController@update')->middleware('auth.perm:edit_products');

		Route::get('/product/{id}','ProductsController@show')->middleware('auth.perm:view_products');
		Route::delete('/product/{id}','ProductsController@destroy')->middleware('auth.perm:edit_products');

	//Aplicaciones
		Route::get('/applications/{id}','ApplicationsController@index')->middleware('auth.perm:view_applications');
		Route::get('/application/{id}','ApplicationsController@show')->middleware('auth.perm:edit_applications');
		Route::post('/application','ApplicationsController@store')->middleware('auth.perm:edit_applications');
		Route::post('/application/{id}','ApplicationsController@update')->middleware('auth.perm:edit_applications');
		Route::delete('/applications','ApplicationsController@destroyMultiple')->middleware('auth.perm:edit_applications');
		Route::delete('/application/{id}','ApplicationsController@destroy')->middleware('auth.perm:edit_applications');




 /*
	Route::get('/products/automotive','ProductsController@automotiveIndex')->middleware('auth.perm:view_products');
	Route::get('/products/industrial','ProductsController@industrialIndex')->middleware('auth.perm:view_products');
	Route::get('/products/heavy','ProductsController@heavyIndex')->middleware('auth.perm:view_products');

	Route::post('/product/automotive/store','ProductsController@automotiveStore')->middleware('auth.perm:edit_products');
	Route::post('/product/industrial/store','ProductsController@industrialStore')->middleware('auth.perm:edit_products');
	Route::post('/product/heavy/store','ProductsController@heavyStore')->middleware('auth.perm:edit_products');

	Route::post('/product/automotive/update/{id}','ProductsController@automotiveUpdate')->middleware('auth.perm:edit_products');
	Route::post('/product/industrial/update/{id}','ProductsController@industrialUpdate')->middleware('auth.perm:edit_products');
	Route::post('/product/heavy/update/{id}','ProductsController@heavyUpdate')->middleware('auth.perm:edit_products');*/

	//Marcas
	Route::get('/banners/','BannersController@index')->middleware('auth.perm:view_banners');
	Route::get('/banner/{id}','BannersController@show')->middleware('auth.perm:view_banners');
	Route::post('/banner/','BannersController@store')->middleware('auth.perm:edit_banners');
	Route::post('/banner/{id}','BannersController@update')->middleware('auth.perm:edit_banners');
	Route::delete('/banner/{id}','BannersController@destroy')->middleware('auth.perm:edit_banners');
	Route::delete('/banners/','BannersController@destroyMultiple')->middleware('auth.perm:edit_banners');

	//Articulos
 	Route::get('/articles/','ArticlesController@index')->middleware('auth.perm:view_articles');
	Route::get('/article/{id}','ArticlesController@show')->middleware('auth.perm:view_articles');
	Route::post('/article/','ArticlesController@store')->middleware('auth.perm:edit_articles');
	Route::post('/article/{id}','ArticlesController@update')->middleware('auth.perm:edit_articles');
	Route::delete('/article/{id}','ArticlesController@destroy')->middleware('auth.perm:edit_articles');
	Route::delete('/articles/','ArticlesController@destroyMultiple')->middleware('auth.perm:edit_articles');

	//Categorias
	Route::get('/categories/','CategoriesController@index')->middleware('auth.perm:view_subcategories');
	Route::get('/category/{id}','CategoriesController@show')->middleware('auth.perm:view_subcategories');
	Route::post('/category/','CategoriesController@store')->middleware('auth.perm:edit_subcategories');
	Route::post('/category/{id}','CategoriesController@update')->middleware('auth.perm:edit_subcategories');
	Route::delete('/category/{id}','CategoriesController@destroy')->middleware('auth.perm:edit_subcategories');
	Route::delete('/categories/','CategoriesController@destroyMultiple')->middleware('auth.perm:edit_subcategories');

	Route::get('/categorias','CategoriesController@categorias')->middleware('auth.role:administrador');
	Route::get('/categoria/{id}','CategoriesController@mostrarCategoria')->middleware('auth.role:administrador');
	Route::post('/categoria/{id}','CategoriesController@actualizarCategoria')->middleware('auth.role:administrador');


	//Subcategorias
	Route::get('/subcategories/category/name/{category}','SubCategoriesController@categoryShow')->middleware('auth.perm:view_subcategories');

	Route::get('/subcategories/','SubCategoriesController@index')->middleware('auth.perm:view_subcategories');
	Route::get('/subcategories/category/{id}','SubCategoriesController@showCategory')->middleware('auth.perm:view_subcategories');	
	Route::get('/subcategory/{id}','SubCategoriesController@show')->middleware('auth.perm:view_subcategories');
	Route::post('/subcategory/','SubCategoriesController@store')->middleware('auth.perm:edit_subcategories');
	Route::post('/subcategory/{id}','SubCategoriesController@update')->middleware('auth.perm:edit_subcategories');
	Route::delete('/subcategory/{id}','SubCategoriesController@destroy')->middleware('auth.perm:edit_subcategories');
	Route::delete('/subcategories/','SubCategoriesController@destroyMultiple')->middleware('auth.perm:edit_subcategories');

	//Marcas
	Route::get('/carruseles/','CarruselesController@index')->middleware('auth.perm:view_products');
	Route::get('/carrusel/{id}','CarruselesController@show')->middleware('auth.perm:view_products');
	Route::post('/carrusel/','CarruselesController@store')->middleware('auth.perm:edit_products');
	Route::post('/carrusel/{id}','CarruselesController@update')->middleware('auth.perm:edit_products');
	Route::delete('/carrusel/{id}','CarruselesController@destroy')->middleware('auth.perm:edit_products');
	Route::delete('/carruseles/','CarruselesController@destroyMultiple')->middleware('auth.perm:edit_products');
});